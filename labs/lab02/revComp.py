DNAstring = raw_input("Please enter a DNA string: ")
complementstring = ""
for x in DNAstring[:]:
    if x == "A":
        complementstring += 'T'
    elif x == "T":
        complementstring += 'A'
    elif x == "G":
        complementstring += 'C'
    elif x == "C":
        complementstring += 'G'

print "The reverse complement string to " + str(DNAstring) + " is " + \
    str(complementstring[::-1])
