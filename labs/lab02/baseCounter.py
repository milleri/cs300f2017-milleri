DNAstring = raw_input("Please enter a DNA string: ")

numA = DNAstring.count("A")
print "Number of A's: " + str(numA)

numC = DNAstring.count("C")
print "Number of C's: " + str(numC)

numT = DNAstring.count("T")
print "Number of T's: " + str(numT)

numG = DNAstring.count("G")
print "Number of G's: " + str(numG)
