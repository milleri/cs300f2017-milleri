from Bio.SeqUtils import GC
winSize = 5


def getGcContent(in_str):
    print "Getting GC content."
    return GC(in_str)
#end of getGcContent

def slidingWindow(in_str):
    print "This is a slidingWindow() method."
    gcContent_dic = {} # define the dictionary
    for i in range(0,len(in_str), winSize):
        #print i, in_str[i:i+winSize]
        chunk_str = in_str[i:i+winSize]
        gcContent_dic = (chunk_str)
    return gcContent_dic
#end of slidingWindow()

def begin():
    # driver method of the program
    seq_str = "ATACGATCGAGCGAATATTTCG"
    gcContent_dic = slidingWindow(seq_str)
    print "GC Content: ",gcContent_dic
    #end of the begin()
